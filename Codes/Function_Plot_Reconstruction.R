# Plot Reconstruction of Time series and compare with empirical data

Plot_Reconstruction_time_series<-function(Data_frame_1, Data_frame_2,Pars)
{
  
  with(as.list(c(Data_frame_1, Data_frame_2,Pars)), {
    
    time_series_Post_Codes<-Data_frame_1
    empirical_time_series<-Data_frame_2
    file_name_ts<-Pars[1]
    file_name_yearly<-Pars[2]
    y_lab_text<-Pars[3]
    
colnames(time_series_Post_Codes) <-c("Date","Cases","Lambda","PostCode")
time_series<-aggregate (Cases ~ Date, time_series_Post_Codes, sum)  # get the mean of the cases per day for all the PC


time_series_quantile <-ddply(time_series,~Date, function (x) quantile(x$Cases, c(.25,.5,.75)))
time_series_summary <-data.frame(time_series_quantile, rep("Model",times=length(time_series[,1]))) 
colnames(time_series_summary)<-c("Date","f_quant","Cases","s_quant","source")

real_cases_mean <-empirical_time_series %>% mutate (rolling_mean= slider::slide_mean(Cases, before=7 , after=0 )) # %>% ungroup()  ##rolling mean to aremove the weekend effects
empirical_time_series$rolling_mean <-real_cases_mean$rolling_mean  
real_cases_quantile<-ddply(empirical_time_series,~Date, function (x) quantile(x$rolling_mean, c(.25,.5,.75)))

#real_cases_quantile<-ddply(empirical_time_series,~Date, function (x) quantile(x$Cases, c(.25,.5,.75)))
real_cases_summary<-cbind(real_cases_quantile,rep("Cases",times=length(real_cases_quantile[,1])))
colnames(real_cases_summary)<-c("Date","f_quant","Cases","s_quant","source")              

#time_series_all <-rbind(time_series_summary[,c(1,2,6)], real_cases_summary[,c(1,2,6)])

time_series_all <-rbind(time_series_summary, real_cases_summary)


############## Plot Average per day of the year #################

pal<-wes_palette("Cavalcanti1")
time_series_plot<-ggplot(time_series_all,aes(x=Date,y=Cases,colour=source))
time_series_plot<-time_series_plot+ theme_bw(20)
#time_series_plot<-time_series_plot+scale_color_brewer(palette=pal)
#time_series_plot<-time_series_plot+geom_line(data=real_cases_summary)
time_series_plot<-time_series_plot+geom_line(size=0.75)
#time_series_plot<-time_series_plot+scale_color_manual(values=c( "#E69F00", "#56B4E9"))
time_series_plot<-time_series_plot+scale_color_manual(values=wes_palette(n=2, name="Cavalcanti1"))

time_series_plot<-time_series_plot+   xlab("Date")+   ylab("Campylobacteriosis")

time_series_plot<-time_series_plot+
  theme(legend.position= c(0.125,0.85),legend.title =  element_blank(),
        legend.text = element_text( size = 10),legend.background = element_blank(),
        legend.key=element_rect(colour=NA,fill=NA))


time_series_plot<-time_series_plot+ theme(axis.title.x =element_text( colour="#990000", size=13))
time_series_plot<-time_series_plot+ theme(axis.title.y =element_text( colour="#990000", size=13))



time_series_plot<-time_series_plot+ theme(axis.title.x =element_text(size=13))
time_series_plot<-time_series_plot+ theme(axis.title.y =element_text(size=13))
print(time_series_plot)


tiff(filename = file_name_ts,width = 17.35, height =  17.35, units = "cm", pointsize = 9, res = 600,compression = "lzw",antialias="default")
print(time_series_plot)
dev.off()


time_series$yday <-as.factor(yday(time_series$Date))

time_series_average1 <-ddply(time_series,~yday, summarise, mean=mean(Cases)) # mean of all cases for the same day of the year
time_series_quantile1 <-ddply(time_series,~yday, function (x) quantile(x$Cases, c(.25,.5,.75))) # quantiles of cases for all the years for the same day of the year
time_series_average <-cbind(time_series_average1,time_series_quantile1[,-1])

#real_cases$norm<-(real_cases$Cases/sum(real_cases$Cases))
empirical_time_series$yday<-as.factor(yday(as.Date(as.character(empirical_time_series$Date))))
real_cases_average1<-ddply(empirical_time_series,~yday,summarise,mean=mean(Cases))
real_cases_quantile1<-ddply(empirical_time_series,~yday,function (x) quantile(x$Cases, c(.25,.5,.75)))
real_cases_average2<-cbind(real_cases_average1,real_cases_quantile1[,-1])
real_cases_average<-
  data.frame(real_cases_average2[,1],real_cases_average2[,c(2:5)])


df1 <-data.frame(real_cases_average,rep("Cases",times=length(real_cases_average[,1])))
colnames(df1) <-c("Day","Mean","f_quant","median","s_quant","source")
df2 <-data.frame(time_series_average,rep("Model",times=length(time_series_average[,1])))
colnames(df2) <-c("Day","Mean","f_quant","median","s_quant","source")

average_data <-rbind(df1,df2)
average_data$Day <-as.numeric(average_data$Day)

## We get one of the years as example to plot the date, in this case 2014, but could be any year within our data set.
month_2014 <-month.abb[unique(month(as.Date(average_data$Day, origin = "2014-01-01")))]

tick_pos<-yday(as.Date(as.character(
  c("2014-01-01","2014-02-01","2014-03-01","2014-04-01","2014-05-01","2014-06-01",
    "2014-07-01","2014-08-01","2014-09-01","2014-10-01","2014-11-01","2014-12-01")
)))



yearly_average <-ggplot(average_data, aes(x=Day, y=Mean, colour=source))+ theme_bw(20)+
  geom_line(size=2)+
  geom_ribbon(aes(ymin=f_quant, ymax=s_quant, fill=source), alpha=0.15) +
  xlab("Day of the Year") + ylab(y_lab_text) +
  
  scale_x_continuous(breaks=tick_pos, labels=month_2014) +
  
  theme(legend.position= c(0.125,0.75),legend.title =  element_text( size = 10),
        legend.text = element_text( size = 10),legend.background = element_blank(),
        legend.key=element_rect(colour=NA,fill=NA)) +
  
  theme(axis.title.x =element_text( colour="#990000", size=13)) +
  theme(axis.title.y =element_text( colour="#990000", size=13)) 
#+  ggtitle(paste(i,",", h,",", k))


print(yearly_average)


# Save the plots:
# Yearly average

tiff(filename = file_name_yearly, width = 17.35, height =  17.35, units = "cm", pointsize = 9, res = 600,compression = "lzw", antialias="default")
print(yearly_average)
dev.off()

return()

})
}
